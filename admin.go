package fortigate

// Admin users

const AdminUserPath = "cmdb/system/admin"

type AdminUser struct {
	Name        string         `json:"name,omitempty"`
	Comment     string         `json:"comment,omitempty"`
	RemoteAuth  string         `json:"remote-auth,omitempty"` // enable or disable
	Password    string         `json:"password,omitempty"`
	VDOM        []LinkedObjRef `json:"vdom,omitempty"`
	SSHKey1     string         `json:"ssh-public-key1,omitempty"`
	RemoteGroup string         `json:"remote-group,omitempty"`
	AccProfile  string         `json:"accprofile,omitempty"` // access profile
}

func (a AdminUser) GetKey() string {
	return a.Name
}

func (a AdminUser) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{
		BasePath: AdminUserPath,
	}
}
