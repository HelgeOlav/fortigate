package fortigate

import (
	"testing"
)

func TestNewLinkedObjRefFromStrings(t *testing.T) {
	input := []string{"january, february"}
	result := NewLinkedObjRefFromStrings(input)
	if len(result) != len(input) {
		t.Errorf("NewLinkedObjRefFromStrings does not create exepcted amount of entries")
	}
}

func TestRemoveDuplicates(t *testing.T) {
	input := []string{"a", "b", "b"}
	result := RemoveDuplicates(input)
	if len(result) != 2 {
		t.Errorf("RemoveDuplicates expected 2 in return")
	}
	result = RemoveDuplicates(nil)
	if result != nil {
		t.Errorf("RemoveDuplicates expected nil output on nil input")
	}
	result = RemoveDuplicates([]string{})
	if len(result) != 0 {
		t.Errorf("RemoveDuplicates expected empty output on empty input")
	}
}

func TestFortigateResult_GetKey(t *testing.T) {
	res := FortigateResult{}
	res.Key = "3"
	if res.GetKey() != "3" {
		t.Errorf("GetKey sent string 3, got %s", res.GetKey())
	}
	res.Key = 5
	if res.GetKey() != "5" {
		t.Errorf("GetKey sent int 5, got %s", res.GetKey())
	}
	res.Key = nil
	if res.GetKey() != "" {
		t.Errorf("GetKey sent nil, did not get blank string")
	}
	var myFloat float64 = 12
	res.Key = myFloat
	if res.GetKey() != "12" {
		t.Errorf("GetKey sent float 12, got %s", res.GetKey())
	}
}
