package fortigate

import "strconv"

const StaticRoutePath = "cmdb/router/static"

// StaticRoute is static route configuration object
type StaticRoute struct {
	SeqNum     int    `json:"seq-num,omitempty"`
	QOriginKey int    `json:"q_origin_key,omitempty"`
	Status     string `json:"status,omitempty"` // enable or disable (defaults to enable if not set)
	Dst        string `json:"dst"`              // 1.2.3.4 255.255.255.0 (input can be CIDR-format)
	Src        string `json:"src,omitempty"`
	Gateway    string `json:"gateway"`            // IP of next hop
	Distance   int    `json:"distance,omitempty"` // defaults to 10 if not set on creation
	Weight     int    `json:"weight,omitempty"`
	Priority   int    `json:"priority,omitempty"`
	Device     string `json:"device"`
	Comment    string `json:"comment,omitempty"`
	Blackhole  string `json:"blackhole,omitempty"` // enable or disable
	VRF        int    `json:"vrf,omitempty"`
}

func (s StaticRoute) GetKey() string {
	return strconv.Itoa(s.QOriginKey)
}

func (s StaticRoute) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: StaticRoutePath}
}
