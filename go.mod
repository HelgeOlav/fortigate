module bitbucket.org/HelgeOlav/fortigate

go 1.17

require github.com/mitchellh/mapstructure v1.4.3

require bitbucket.org/HelgeOlav/utils v0.0.0-20211209222926-7771de6afdcd // indirect
