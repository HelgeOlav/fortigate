# Fortigate client library

This library implements calls to the Fortigate API to make it easier for you to work with the Fortigate.

To get started you need a Connection object
```golang
c := fortigate.Connection{
 BaseURL: "https://myfortigate/api/v2",
 Token: "token-from-rest-api",
 VDOM: "root",
 InsecureSSL: true,
}
```

To get a list of all objects just do a query

```golang
result, err := c.GetAddressObjects(fortigate.NewParam())
```

## Having a persistent connection on disk

If you don't want to create an object as shown above, then create a JSON file
```json
{
  "baseurl": "https://myfortigate/api/v2",
  "token": "rest-api-token",
  "vdom": "root",
  "insecure-ssl": true
}
```
And then load it from disk:
```golang
c, _ := fortigate.LoadConnection("filename")
```

It is also possible to try get the Connection object automatically.

```golang
c, := fortigate.DefaultConnection()
```
This way the connection is first attempted to be configured by the environment, then by loading a file ```conn.json```
in the current directory. The file has the layout as shown above.

For the environment, the following variables must be set:
```shell
FORTIOS_ACCESS_HOSTNAME=1.1.1.1
FORTIOS_INSECURE=true
FORTIOS_ACCESS_TOKEN=mytoken
```