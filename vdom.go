package fortigate

// Work with VDOM

const VDOMPath = "cmdb/system/vdom"

type VDOM struct {
	Name      string `json:"name,omitempty"`
	ShortName string `json:"short-name,omitempty"`
	ClusterID int    `json:"vcluster-id,omitempty"`
	Flag      int    `json:"flag,omitempty"`
}

func (V VDOM) GetKey() string {
	return V.Name
}

func (V VDOM) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: VDOMPath}
}
