package fortigate

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// LinkedObjRef is used many places when referencing other objects on the firewall
type LinkedObjRef struct {
	Name   string `json:"name,omitempty"`           // used in most places
	IfName string `json:"interface-name,omitempty"` // used on zones
	Origin string `json:"q_origin_key,omitempty"`   // used all places (seen), but must be used with one of the above
}

// GenericResult defines the result we get from most query operations.
// Not all fields are returned every time.
type GenericResult struct {
	Method           string `json:"http_method"`
	Revision         string
	OldRevision      string `json:"old_revision,omitempty"`
	VDOM             string
	Path             string
	Name             string
	Status           string
	Error            int
	HTTPResponseCode int `json:"http_status"`
	Serial           string
	Version          string
	Build            int
	Changed          bool        `json:"revision_changed,omitempty"`
	CLIError         string      `json:"cli_error"`
	Key              interface{} `json:"mkey,omitempty"`
}

// GetKey returns mkey/key from POST. This key are returned from the Fortgate as either integer and string depending on context.
// You need this key when you want to get a newly created object.
func (result *FortigateResult) GetKey() (key string) {
	switch result.Key.(type) {
	case string:
		key = result.Key.(string)
	case int:
		key = strconv.Itoa(result.Key.(int))
	case float64, float32:
		key = fmt.Sprintf("%.0f", result.Key)
	}
	return
}

// FortigateResult is a more generic way of describing the result from a Fortigate
// This struct was introduced to try reduce the amount repeating code.
type FortigateResult struct {
	GenericResult
	Result interface{} `json:"results"`
}

// errorIfError returns error if there is an error in the result otherwise nil.
// This method is used to safely check if there is an error and return one if so.
func (res *FortigateResult) errorIfError() error {
	if res != nil {
		if res.HTTPResponseCode > 0 && res.HTTPResponseCode < 200 && res.HTTPResponseCode >= 300 {
			return res
		}
	}
	return nil
}

// Error makes it possible for FortigateResult to be its own error giving the error based on on the content of the result.
func (res *FortigateResult) Error() string {
	// handle nil value
	if res == nil {
		return "nil passed into FortigateResult.Error()"
	}
	// see if there is an error
	if res.Status == "success" {
		return "no error in FortigateResult"
	}
	// try to make something out of the error
	var sb strings.Builder
	const space = ' '
	const slash = '/'
	// add error code and method
	_, _ = sb.WriteString(strconv.Itoa(res.HTTPResponseCode))
	_, _ = sb.WriteRune(space)
	_, _ = sb.WriteString(res.Method)
	_, _ = sb.WriteRune(space)
	// add path if it is there
	if len(res.Path) > 0 {
		_, _ = sb.WriteString(res.VDOM)
		_, _ = sb.WriteRune(slash)
		_, _ = sb.WriteString(res.Path)
		_, _ = sb.WriteRune(slash)
		_, _ = sb.WriteString(res.Name)
		k := res.GetKey()
		if len(k) > 0 {
			_, _ = sb.WriteRune(slash)
			_, _ = sb.WriteString(k)
		}
		if len(res.CLIError) > 0 {
			_, _ = sb.WriteRune(space)
			_, _ = sb.WriteString(res.CLIError)
		}
	}
	return sb.String()
}

// FortiObjectUpdatable interface is used when an object is writable (supports delete/put)
type FortiObjectUpdatable interface {
	GetKey() string                   // return object key, typically referred to as mkey
	GetMetaData() FortiObjectMetadata // return metadata
}

// ParamHandler is a func definition that is used with FortiObjectMetadata to make it possible to put
// handlers into different Fortigate objects. It returns Param and an error if not ok to continue the process.
// This way a handler can block an operation before it is done.
type ParamHandler func(Param) (Param, error)

// FortiObjectMetadata is defined for each Fortigate object and contains the metadata we need to handle
// read and writes for a given object.
type FortiObjectMetadata struct {
	BasePath        string       // the path to the object not including leading and trailing slashes
	ParamGetHook    ParamHandler // if set the passed Param is sent through this handler for GET operations
	ParamDeleteHook ParamHandler // if set the passed Param is sent through this handler for DELETE operations
	ParamUpdateHook ParamHandler // if set the passed Param is sent through this handler for PUT operations
	ParamCreateHook ParamHandler // if set the passed Param is sent through this handler for POST operations
}

// FortiObjectReadable defines a Fortigate object that can be read from
type FortiObjectReadable interface {
	GetMetaData() FortiObjectMetadata
}

// NewLinkedObjRefFromStrings is a helper that returns a list of LinkedObjRef with the name
// field populated from the input string array. It also removes duplicate entries from the input.
func NewLinkedObjRefFromStrings(input []string) []LinkedObjRef {
	input = RemoveDuplicates(input)
	var result []LinkedObjRef = []LinkedObjRef{}
	if len(input) > 0 {
		for _, v := range input {
			entry := LinkedObjRef{Name: v}
			result = append(result, entry)
		}
	}
	return result
}

// NewIFLinkedObjRefFromStrings is a helper that returns a list of LinkedObjRef with the name
// field populated from the input string array. It also removes duplicate entries from the input.
func NewIfLinkedObjRefFromStrings(input []string) []LinkedObjRef {
	input = RemoveDuplicates(input)
	var result []LinkedObjRef = []LinkedObjRef{}
	if len(input) > 0 {
		for _, v := range input {
			entry := LinkedObjRef{IfName: v}
			result = append(result, entry)
		}
	}
	return result
}

// RemoveDuplicates takes an input array and return an array without any duplicate entries in it.
func RemoveDuplicates(input []string) []string {
	if input == nil {
		return nil
	}
	var list map[string]int = make(map[string]int)
	for _, k := range input {
		list[k] = 1
	}
	var output []string
	for k, _ := range list {
		output = append(output, k)
	}
	return output
}

// methodNotAllowed is an internal helper that blocks method calls that are not allowed.
func methodNotAllowed(p Param) (Param, error) {
	return p, errors.New("HTTP method not allowed")
}
