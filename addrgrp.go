package fortigate

import (
	"errors"
	"net/http"
	"strconv"
)

const AddrGrpPath = "cmdb/firewall/addrgrp"

// AddrGrpResult is the result from GET requests
type AddrGrpResult struct {
	GenericResult
	Result []AddrGrp `json:"results"`
}

// FindByName look up a group by its name
func (a AddrGrpResult) FindByName(name string) AddrGrp {
	res := AddrGrp{}
	for _, v := range a.Result {
		if v.Name == name {
			return v
		}
	}
	return res
}

// AddrGrp is an address group
type AddrGrp struct {
	Name    string         `json:"name,omitempty"`
	Origin  string         `json:"q_origin_key,omitempty"`
	UUID    string         `json:"uuid,omitempty"`
	Member  []LinkedObjRef `json:"member"`
	Comment string         `json:"comment,omitempty"`
	Color   int            `json:"color,omitempty"`
}

//TODO: check if FortiObject* methods works on AddrGrp (and perhaps Addr)

func (a AddrGrp) GetKey() string {
	return a.Name
}

func (a AddrGrp) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: AddrGrpPath}
}

// GetAddressGroups returns a list of address objects from the firewall
func (c *Connection) GetAddressGroups(p Param) (AddrGrpResult, error) {
	var result AddrGrpResult
	err := c.DoAndParseRequest(AddrGrpPath, http.MethodGet, p, nil, &result)
	if err == nil {
		if result.HTTPResponseCode != http.StatusOK {
			err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
		}
	}
	return result, err
}

// DeleteAddressGroupObject will delete the specified object by its mkey
func (c *Connection) DeleteAddressGroupObject(mkey string) error {
	if c == nil {
		return errors.New("DeleteAddressGroupObject Connection==nil")
	}
	p := NewParam().SetMkey(mkey)
	req, err := c.NewRequest(http.MethodDelete, AddrGrpPath, p, nil)
	if err != nil {
		return err
	}
	res, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	_ = res.Body.Close()
	if res.StatusCode == http.StatusOK {
		return nil
	}
	return errors.New("DeleteAddressGroupObject failed somehow") //TODO: perhaps fix error handling?
}

// DeleteGroupAddress deletes the address specified by AddrGrp
func (c *Connection) DeleteGroupAddress(a AddrGrp) error {
	return c.DeleteAddressGroupObject(a.Origin)
}

// CreateAddressGroupObject create a new address object specified by an AddrGrp
func (c *Connection) CreateAddressGroupObject(a AddrGrp) error {
	if c == nil {
		return errors.New("CreateAddressGroupObject Connection==nil")
	}
	var result GenericResult
	err := c.DoAndParseRequest(AddrGrpPath, http.MethodPost, NewParam(), a, &result)
	if err != nil {
		return err
	}
	if result.HTTPResponseCode != http.StatusOK {
		errMsg := strconv.Itoa(result.HTTPResponseCode) + " " + result.Status + " " + strconv.Itoa(result.Error)
		return errors.New(errMsg)
	}
	return nil
}

// UpdateAddressGroupObject updates an existing address object
func (c *Connection) UpdateAddressGroupObject(a AddrGrp) error {
	if c == nil {
		return errors.New("UpdateAddressGroupObject Connection==nil")
	}
	var result GenericResult
	err := c.DoAndParseRequest(AddrGrpPath, http.MethodPut, NewParam().SetMkey(a.Origin), a, &result)
	if err != nil {
		return err
	}
	if result.HTTPResponseCode != http.StatusOK {
		errMsg := strconv.Itoa(result.HTTPResponseCode) + " " + result.Status + " " + strconv.Itoa(result.Error)
		return errors.New(errMsg)
	}
	return nil
}
