package fortigate

import "bitbucket.org/HelgeOlav/utils/version"

const Version = "0.0.1"

func init() {
	version.AddModule(version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/fortigate", Version: Version})
}
