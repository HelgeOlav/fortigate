package fortigate

import (
	"errors"
	"net/http"
	"strconv"
)

// Looking into the routing table

const Routing4Path = "monitor/router/ipv4"
const Routing6Path = "monitor/router/ipv6"

// Route represents both IPv4 and IPv6 routes
type Route struct {
	IP_Version int    `json:"ip_version"`
	Type       string `json:"type"`
	Prefix     string `json:"ip_mask"`
	Distance   int    `json:"distance"`
	Metric     int    `json:"metric"`
	Priority   int    `json:"priority"`
	VRF        int    `json:"vrf"`
	Gateway    string `json:"gateway"`
	Interface  string `json:"interface"`
}

// RouteResult is the result from the query
type RouteResult struct {
	GenericResult
	Result []Route `json:"results"`
}

// GetAllRoute4 returns all IPv4 routes from the router
func (c *Connection) GetAllRoute4(p Param) (RouteResult, error) {
	var result RouteResult
	err := c.DoAndParseRequest(Routing4Path, http.MethodGet, p, nil, &result)
	if err == nil {
		if result.HTTPResponseCode > 0 && result.HTTPResponseCode != http.StatusOK {
			err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
		}
	}
	return result, err
}

// GetAllRoute6 returns all IPv6 routes from the router
func (c *Connection) GetAllRoute6(p Param) (RouteResult, error) {
	var result RouteResult
	err := c.DoAndParseRequest(Routing6Path, http.MethodGet, p, nil, &result)
	if err == nil {
		if result.HTTPResponseCode > 0 && result.HTTPResponseCode != http.StatusOK {
			err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
		}
	}
	return result, err
}

// GetAllRoutes returns both IPv4 and IPv6 routes in one go
func (c *Connection) GetAllRoutes(p Param) ([]Route, error) {
	var routes []Route = []Route{}
	res4, err4 := c.GetAllRoute4(p)
	res6, err6 := c.GetAllRoute6(p)
	routes = append(res4.Result, res6.Result...)
	if err4 != nil {
		return routes, err4
	}
	return routes, err6
}
