package zonemapper

import (
	"testing"
)

func TestRoutemapper_FindZone(t *testing.T) {
}

func TestLoadFromFile(t *testing.T) {
	r, err := LoadFromFile("routes.json")
	if err != nil {
		t.Errorf("LoadFromFile error %s", err)
	}
	if r.DefaultZone != "outside" {
		t.Errorf("LoadFromFile got wrong default zone")
	}
	if len(r.Prefixes) != 5 {
		t.Errorf("LoadFromFile expected five Prefixes")
	}
	lastPrefix := 129
	// check that the list is sorted down
	for _, v := range r.Prefixes {
		mask, _ := v.Net.Mask.Size()
		if mask > lastPrefix {
			t.Errorf("LoadFromFile does not sort correctly")
			break
		} else {
			lastPrefix = mask
		}
	}
}

func TestRoutemapper_FindAllMatchingPrefixes(t *testing.T) {
	r, err := LoadFromFile("routes.json")
	if err != nil {
		t.Errorf("LoadFromFile error %s", err)
	}
	// check for two overlapping prefixed for a given source IP
	res := r.FindAllMatchingPrefixes("10.254.254.0/24")
	if len(res) != 2 {
		t.Error("FindAllMatchingPrefixes expected two results, got", res)
	}
	// check for no overlapping prefixes, only one result
	res = r.FindAllMatchingPrefixes("192.168.0.5")
	if len(res) != 1 {
		t.Error("FindAllMatchingPrefixes expected one result, got", res)
	}
	// check for no prefix (should be default that is not returned)
	res = r.FindAllMatchingPrefixes("1.1.1.1")
	if len(res) != 0 {
		t.Error("FindAllMatchingPrefixes expected empty result, got", res)
	}
}
