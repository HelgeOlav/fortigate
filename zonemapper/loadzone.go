package zonemapper

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"net"
)

// GetZoneMapFromRoutes returns a Routemapper based on the current routing table, zones and interfaces on the Fortigate.
// If the defaultZone is empty the default IPv4 route will be used instead.
func GetZoneMapFromRoutes(c *fortigate.Connection, defaultZone string) (Routemapper, error) {
	var rm Routemapper
	p := fortigate.NewParam()
	// get interfaces
	ifList, err := c.GetIntfs(p)
	if err != nil {
		return rm, err
	}
	// get zones
	zr, err := c.GetFortiObject(p, fortigate.Zone{})
	if err != nil {
		return rm, err
	}
	var zones []fortigate.Zone
	err = fortigate.Interface2Object(zr.Result, &zones)
	if err != nil {
		return rm, err
	}
	// get interface map
	ifmap := fortigate.GetZonesAsIfMap(zones, ifList.Result)
	// get routes for IPv4
	routes, err := c.GetAllRoutes(p)
	if err != nil {
		return rm, err
	}
	// output to rm
	for _, v := range routes {
		_, ourNet, err := net.ParseCIDR(v.Prefix)
		if err == nil {
			prefix := Prefix{
				Prefix: v.Prefix,
				Net:    *ourNet,
				Zone:   ifmap[v.Interface],
			}
			if len(prefix.Zone) > 0 {
				rm.Prefixes = append(rm.Prefixes, prefix)
			}
		}
	}
	// set default zone
	if len(defaultZone) > 0 {
		rm.DefaultZone = defaultZone
	} else {
		defgw := rm.FindZonesFromIP("0.0.0.0")
		if len(defgw) == 1 {
			rm.DefaultZone = defgw[0]
		}
	}
	// and we are done
	rm.Sort()
	return rm, err
}
