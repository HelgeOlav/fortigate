package zonemapper

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"sort"
	"strings"
)

// Prefix is a route and its zone
type Prefix struct {
	Prefix string    `json:"prefix"` // IP Prefix in CIDR format
	Net    net.IPNet `json:"-"`      // Prefix in IPNet format
	Zone   string    `json:"zone"`   // zone to map to
}

// Routemapper is a helper method that maps IP addresses to zones based on existing routes.
// To use this you first need to load in a map of zones and routes.
type Routemapper struct {
	DefaultZone string   `json:"default-zone"`          // default zone when there is no match, typically pointing towards the Internet
	DebugLevel  int      `json:"debug-level,omitempty"` // 0=none, 1=print default zone matches, 2=print all matches
	Prefixes    []Prefix `json:"prefixes"`              // ordered list of prefixed to match on
}

// RoutesWithZone is top-level JSON import, see README.md for syntax.
type RoutesWithZone struct {
	DefaultZone string              `json:"defaultzone"`
	Zones       map[string][]string `json:"zones,omitempty"`
}

// FindZones returns a list of zones based on input IP
func (r *Routemapper) FindZones(IP []string) []string {
	zones := []string{}
	for _, v := range IP {
		zones = append(zones, r.FindZonesFromIP(v)...)
	}
	return fortigate.RemoveDuplicates(zones)
}

// FindAllMatchingPrefixes returns all prefixes that matches instead of just the first (best) match.
// This is useful for finding overlapping entries. Default zone is not matched here.
func (r *Routemapper) FindAllMatchingPrefixes(IP string) []string {
	pf := []string{}
	if r != nil {
		if strings.Contains(IP, "/") {
			split := strings.Split(IP, "/")
			IP = split[0]
		}
		// convert IP, return with empty list if it failed
		ip := net.ParseIP(IP)
		if ip == nil {
			return pf
		}
		// start searching through prefixes
		for k, v := range r.Prefixes {
			if v.Net.Contains(ip) {
				if r.DebugLevel > 1 {
					fmt.Println("routemapper:", ip, "mapped to", k)
				}
				pf = append(pf, v.Prefix)
			}
		}
	}
	return pf
}

// FindZone looks through each route to see if it is a match
// Deprecated: use FindZonesFromIP instead
func (r *Routemapper) FindZone(IP string) string {
	// do we exist?
	if r == nil {
		return ""
	}
	// do we have any routes map?
	if len(r.Prefixes) == 0 {
		return ""
	}
	// if input contains Prefix, remove that part
	if strings.Contains(IP, "/") {
		split := strings.Split(IP, "/")
		IP = split[0]
	}
	// convert IP
	ip := net.ParseIP(IP)
	if ip == nil {
		return ""
	}
	// start searching through zones
	for k, v := range r.Prefixes {
		if v.Net.Contains(ip) {
			if r.DebugLevel > 1 {
				fmt.Println("routemapper:", ip, "mapped to", k)
			}
			return v.Zone
		}
	}
	if r.DebugLevel > 0 {
		fmt.Println("routemapper: Default zone match on", ip)
	}
	return r.DefaultZone
}

// FindZonesFromIP looks through each route to see if it is a match
func (r *Routemapper) FindZonesFromIP(IP string) []string {
	result := []string{}
	// do we exist?
	if r == nil {
		return result
	}
	// do we have any routes map?
	if len(r.Prefixes) == 0 {
		return result
	}
	// if input contains Prefix, remove that part
	if strings.Contains(IP, "/") {
		split := strings.Split(IP, "/")
		IP = split[0]
	}
	// convert IP
	ip := net.ParseIP(IP)
	if ip == nil {
		return result
	}
	// start searching through zones
	for k, v := range r.Prefixes {
		if v.Net.Contains(ip) {
			if r.DebugLevel > 1 {
				fmt.Println("routemapper:", ip, "mapped to", k)
			}
			result = append(result, v.Zone)
		}
	}
	if len(result) == 0 {
		if r.DebugLevel > 0 {
			fmt.Println("routemapper: Default zone match on", ip)
		}
		return []string{r.DefaultZone}
	}
	return fortigate.RemoveDuplicates(result)
}

// LoadFromFile Load route map from file.
func LoadFromFile(fn string) (*Routemapper, error) {
	var jsonRoutes RoutesWithZone
	var routes *Routemapper
	// load file
	bv, err := ioutil.ReadFile(fn)
	if err != nil {
		return routes, err
	}
	// unmarshal
	err = json.Unmarshal(bv, &jsonRoutes)
	if err != nil {
		return routes, err
	}
	// copy over to new object
	routes = new(Routemapper)
	routes.DefaultZone = jsonRoutes.DefaultZone
	ourPrefixes := []Prefix{}
	for zone, prefixes := range jsonRoutes.Zones {
		for _, prefix := range prefixes {
			_, ipnet, err := net.ParseCIDR(prefix)
			if err != nil {
				return routes, err
			}
			pf := Prefix{
				Prefix: prefix,
				Net:    *ipnet,
				Zone:   zone,
			}
			ourPrefixes = append(ourPrefixes, pf)
		}
	}
	sort.Slice(ourPrefixes, func(x, y int) bool {
		mask1, _ := ourPrefixes[x].Net.Mask.Size()
		mask2, _ := ourPrefixes[y].Net.Mask.Size()
		return mask1 > mask2
	})
	routes.Prefixes = ourPrefixes
	return routes, nil
}

// Sort sorts routes after loading so most exact match is placed first in the table.
func (rm *Routemapper) Sort() {
	if rm != nil {
		sort.Slice(rm.Prefixes, func(x, y int) bool {
			mask1, _ := rm.Prefixes[x].Net.Mask.Size()
			mask2, _ := rm.Prefixes[y].Net.Mask.Size()
			return mask1 > mask2
		})
	}
}

// LoadDefaultRoutemap attempts to load a route map from eiher a default file name (routemap.json) in the current directory
// or from the file specified in the environment variable ROUTEMAP
func LoadDefaultRoutemap() (*Routemapper, error) {
	// try envionment first
	env := os.Getenv("ROUTEMAP")
	if len(env) > 0 {
		return LoadFromFile(env)
	}
	// try load from file
	return LoadFromFile("routemap.json")
}

func (rm *Routemapper) SaveToFile(fn string, deleteDefaultZone bool) error {
	if rm == nil {
		return errors.New("nil Routemapper passed to SaveToFile")
	}
	var rz RoutesWithZone
	rz.DefaultZone = rm.DefaultZone
	rz.Zones = make(map[string][]string)
	// create struct
	for _, v := range rm.Prefixes {
		rz.Zones[v.Zone] = append(rz.Zones[v.Zone], v.Prefix)
	}
	// delete content of default zone
	if deleteDefaultZone {
		delete(rz.Zones, rm.DefaultZone)
	}
	// convert to JSON
	bytes, err := json.Marshal(&rz)
	if err != nil {
		return err
	}
	// write file
	return ioutil.WriteFile(fn, bytes, 0644)
}
