# Zonemapper

To aid you in mapping prefixes toward zones you can use zonemapper.

Given this example JSON file:
```json
{
  "defaultzone": "outside",
  "zones": {
    "dmz": [ "192.168.0.0/24", "2001:4644:3e2:0::/64" ],
    "inside": [ "10.0.0.0/8" ]
  }
}
```
Overlapping prefixes are not good as only the most specific match is retuned.

```golang
r, _ := zonemapper.LoadFromFile("routes.json")
prefix := "10.1.1.0/24"
fmt.Println(prefix, "is at zone", r.FindZone(prefix))
```
Based on the JSON file above you will get the result "inside".

It is also possible to create this map by reading the Fortigate runtime data (routing table, zones and interfaces) and use it that way.

```golang
myConn, _ := fortigate.DefaultConnection()
r, _ := zonemapper.GetZoneMapFromRoutes(&myConn, "")
prefix := "10.1.1.0/24"
fmt.Println(prefix, "is at zone", r.FindZone(prefix))
```

The result will depend on the current setup on the firewall.