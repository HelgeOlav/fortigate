package main

import (
	"encoding/json"
	"fmt"
)

type Sub1 struct {
	Name string `json:"name"`
}

type Sub2 struct {
	Age int `json:"age"`
}

type Master struct {
	Comment string      `json:"comment"`
	Data    interface{} `json:"data"`
}

func testStructs() {
	myJson := "{\"comment\":\"test1\",\"data\":{\"name\":\"Helge\",\"age\":10}}"
	m := Master{}
	m.Comment = "test1"
	// m.Data = Sub1{Name: "Helge"}
	m.Data = []Sub1{
		Sub1{Name: "Helge"},
	}
	fmt.Println(m)
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))
	fmt.Println(myJson)
}
