package main

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/fortigate/zonemapper"
	"bitbucket.org/HelgeOlav/utils/version"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
)

func test1() {
	p := fortigate.NewParam()
	p = p.Format("a1").Token("token").Format("f1").Format("f2")
	fmt.Println(p)
}

func test2() {
	//c := fortigate.Connection{BaseURL: "https://1.1.1.1/api/v2", Token: "mytoken", VDOM: "root"}
	c := fortigate.Connection{BaseURL: "https:///13.48.58.0/api/v2", Token: "c9Qtth6qH7tpsw5kQm3rynqwN4dcd3", VDOM: ""}
	p := fortigate.NewParam().Format("myfmt")
	req, err := c.NewRequest(http.MethodGet, "cmdb/something", p, nil)
	fmt.Println(err)
	fmt.Println(req)
}

func test3() {
	c := fortigate.Connection{BaseURL: "https://13.48.58.0/api/v2", Token: "c9Qtth6qH7tpsw5kQm3rynqwN4dcd3", VDOM: ""}
	c.IgnoreSSLCerts()
	result, err := c.GetAddressObjects(fortigate.NewParam().SetSuffix("/helge_test"))
	fmt.Println(err)
	fmt.Println(result)
	// delete something
	err = c.DeleteAddressObject("helge2")
	fmt.Println(err)
}

func getConn() fortigate.Connection {
	c := fortigate.Connection{
		BaseURL: "https://13.48.58.0/api/v2",
		Token:   "c9Qtth6qH7tpsw5kQm3rynqwN4dcd3",
		VDOM:    "root",
	}
	c.IgnoreSSLCerts()
	return c
}

func test4() {
	c := getConn()
	addr := fortigate.Addr{
		Name:   "helge2",
		Subnet: "22.1.1.0 255.255.255.0",
		Type:   "ipmask",
	}
	//fmt.Println(c.CreateAddressObject(addr))
	// get addr
	o, err := c.GetAddressObjects(fortigate.NewParam().SetSuffix("/helge2"))
	if err != nil {
		fmt.Println(err)
		return
	}
	addr = o[0]
	addr.Subnet = "10.10.10.0 255.255.255.0"
	err = c.UpdateAddressObject(addr)
	fmt.Println(err)
	// delete
	fmt.Println(c.DeleteAddress(addr))
}

func test5() {
	c := getConn()
	ag, err := c.GetAddressGroups(fortigate.NewParam())
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(ag)
	a := fortigate.AddrGrp{
		Name: "helge3",
		Member: []fortigate.LinkedObjRef{
			fortigate.LinkedObjRef{
				Name: "gmail.com",
			},
		},
		Comment: "my own group",
		Color:   3,
	}
	err = c.CreateAddressGroupObject(a)
	fmt.Println(err)
	ag, _ = c.GetAddressGroups(fortigate.NewParam())
	a = ag.FindByName("helge3")
	a.Member = []fortigate.LinkedObjRef{
		fortigate.LinkedObjRef{
			Name: "G Suite",
		},
	}
	err = c.UpdateAddressGroupObject(a)
	fmt.Println(err)
	err = c.DeleteGroupAddress(a)
	fmt.Println(err)
}

func test6() {
	c := getConn()
	r, err := c.GetSessions(fortigate.NewParam().Summary())
	fmt.Println(err)
	fmt.Println(r)
}

func test7() {
	c := getConn()
	p := fortigate.NewParam()
	//addr := []fortigate.Addr{}
	//res, err := c.GetFortiObject(p, addr)

	res, err := c.GetAddressObjects(p)
	fmt.Println(err)
	fmt.Println(res)
	fmt.Println(reflect.TypeOf(res))
}

func test8() {
	c := getConn()
	p := fortigate.NewParam()
	allAddrs, err := c.GetFortiObject(p.SetMkey("metadata-server"), []fortigate.Addr{})
	fmt.Println(err)
	fmt.Println(allAddrs)
	// create an object
	sess, err := c.GetFortiObject(p.Summary(), fortigate.SessionResultResult{})
	fmt.Println(err)
	fmt.Println(sess)
	_, err = c.UpdateFortiObject(p, fortigate.SessionResultResult{})
	fmt.Println(err)
}

func test9() {
	c := getConn()
	p := fortigate.NewParam()
	users, err := c.GetFortiObject(p, fortigate.VDOM{})
	fmt.Println(err)
	fmt.Println(users)
	// ---
	newAdmin := fortigate.AdminUser{Name: "helge", Password: "Admin123!", Comment: "min bruker", AccProfile: "super-admin", RemoteAuth: "disable"}
	res, err := c.CreateFortiObject(p, newAdmin)
	fmt.Println(err)
	fmt.Println(res)
}

// test10 test av CLI output
func test10() {
	a := fortigate.Addr{Name: "TEST1", Color: 3, Subnet: "10.0.0.0/8", Comment: "my comment"}
	b := fortigate.Addr{Name: "TEST2", Color: 2, Type: "fqdn", FQDN: "www.vg.no"}
	fmt.Println(a.GetConfigLines())
	fmt.Println(b.GetConfigLines())
}

// test11 Policy
func test11() {
	c := getConn()
	p := fortigate.NewParam()
	pol := fortigate.Policy{
		Name:     "min policy",
		SrcIntf:  fortigate.NewLinkedObjRefFromStrings([]string{"loop1"}),
		DstIntf:  fortigate.NewLinkedObjRefFromStrings([]string{"port1"}),
		SrcAddr:  fortigate.NewLinkedObjRefFromStrings([]string{"all"}),
		DstAddr:  fortigate.NewLinkedObjRefFromStrings([]string{"all"}),
		Action:   "accept",
		Schedule: "always",
		Status:   "enable",
		Service:  fortigate.NewLinkedObjRefFromStrings([]string{"HTTP", "HTTPS"}),
		Comment:  "test fra API",
	}
	b, _ := json.Marshal(pol)
	fmt.Println(string(b))
	res, err := c.CreateFortiObject(p, pol)
	fmt.Println(err, res)
	newKey := res.GetKey()
	fmt.Println("Key", newKey)
	fmt.Println("----- get object -----")
	res, err = c.GetFortiObject(p.SetMkey(newKey), pol)
	fmt.Println(err)
	pol, err = res.GetPolicyFromResult()
	fmt.Println("GetPolicyFromResult error:", err)
	fmt.Println(pol.UUID)
	fmt.Println("----- delete object -----")
	res, err = c.DeleteFortiObject(fortigate.NewParam(), pol)
	fmt.Println(err, res)
}

func test12() {
	c := getConn()
	p := fortigate.NewParam()
	proxy := fortigate.WebProxyPolicy{
		Name:       "minproxy",
		DstIntf:    fortigate.NewLinkedObjRefFromStrings([]string{"port1"}),
		Proxy:      "explicit-web",
		SrcAddr:    fortigate.NewLinkedObjRefFromStrings([]string{"all"}),
		DstAddr:    fortigate.NewLinkedObjRefFromStrings([]string{"all"}),
		Service:    fortigate.NewLinkedObjRefFromStrings([]string{"webproxy"}),
		Action:     "accept",
		Schedule:   "always",
		Status:     "enable",
		LogTraffic: "utm",
		Comment:    "My first service",
	}
	res, err := c.CreateFortiObject(p, proxy)
	newKey := res.GetKey()
	if err == nil {
		fmt.Println("Created key", newKey)
	} else {
		fmt.Println("Create error", err)
		fmt.Println(res.CLIError)
		bytes, _ := json.Marshal(proxy)
		fmt.Println(string(bytes))
	}
	res, err = c.GetFortiObject(p.SetMkey("2"), fortigate.WebProxyPolicy{})
	if err != nil {
		fmt.Println("--- get object error", err)
	} else {
		proxy, err = res.GetWebproxyPolicyFromResult()
		fmt.Println("GetWebproxyPolicyFromResult error:", err)
	}
	fmt.Println(proxy)
	//res, err = c.DeleteFortiObject(p, proxy)
	//fmt.Println(err, res)
}

// test13 test GetZoneMapFromRoutes
func test13() {
	c := getConn()
	rm, err := zonemapper.GetZoneMapFromRoutes(&c, "")
	fmt.Println(err, len(rm.Prefixes), rm)
	fmt.Println(rm.SaveToFile("/Users/helge/temp/zone-out.json", true))
}

func main() {
	ver := version.Get()
	fmt.Println("helge", ver.String())
	test13()
}

func init() {
	version.NAME = "test"
	version.VERSION = fortigate.Version
}
