# fg-deleteall

This tools is a **very** destructive tool created to delete all normal objects on the Fortigate, based on what it can delete.
An object cannot be deleted if it is referenced by another object.

Objects are deleted in this order:

* Firewall policy objects
* Address groups
* Address objects
* Service groups
* Services
* Zones

This tool will continue if there are any failures(except input-failures), the idea is to clean up as much as possible.
Fortigate does not allow you to delete objects that are referenced by other objects.

Some objects are built-in, read only and cannot be deleted at all. So some objects will fail.

## Syntax

```bash
fg-deleteall -url https://my-fg -VDOM root -apikey xxx -dz
```

You only need to specify the url and apikey, the Fortigate will try to assume VDOM. -dz is used to also delete zones.
