package main

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/utils/version"
	"flag"
	"fmt"
	"os"
)

var serverUrl *string        // serverUrl is URL to the Fortigate, syntax "https://192.168.0.1"
var deleteZone *bool         // set if the zones are to be deleted as well
var deleteGroups *bool       // delete address groups
var deleteAddress *bool      // deletes address objects
var deletePolicy *bool       // deletes firewall policies
var deleteServiceGroup *bool // service group
var deleteService *bool      // service
var APIKey *string           // The Fortigate API key
var VDOM *string             // VDOM to work on

type Counter struct {
	Success int
	Fail    int
	Total   int
}

var AddrCounter Counter
var AddrGroupCounter Counter
var PolicyCounter Counter
var ServiceGroupCounter Counter
var ServiceCounter Counter
var ZoneCounter Counter

// parseCmdline Get command line flags, print syntax if needed
func parseCmdline() {
	serverUrl = flag.String("url", "", "Syntax: https://my-fg")
	deleteZone = flag.Bool("dz", false, "set to try delete interface zones")
	deleteAddress = flag.Bool("da", true, "Delete addresses")
	deleteGroups = flag.Bool("dag", true, "delete address groups")
	deleteService = flag.Bool("ds", true, "delete service")
	deleteServiceGroup = flag.Bool("dsg", true, "delete service group")
	deletePolicy = flag.Bool("dp", true, "delete policy")
	APIKey = flag.String("apikey", "", "API key")
	VDOM = flag.String("VDOM", "", "VDOM to modify")
	flag.Parse()
	// check if commands are parsed
	if len(*serverUrl) == 0 || len(*APIKey) == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

var conn fortigate.Connection

func main() {
	ver := version.Get()
	fmt.Printf("%s: the easy way to clean up a firewall\n", ver.String())
	parseCmdline()
	// Create a connection object
	conn = fortigate.Connection{
		BaseURL: *serverUrl + "/api/v2",
		Token:   *APIKey,
		VDOM:    *VDOM,
	}
	conn.IgnoreSSLCerts()
	// delete objects
	if *deletePolicy {
		deleteAllPolicy()
	}
	if *deleteGroups {
		deleteAllHostGroups()
	}
	if *deleteAddress {
		deleteAllHosts()
	}
	if *deleteServiceGroup {
		deleteAllServiceGroups()
	}
	if *deleteService {
		deleteAllServices()
	}
	if *deleteZone {
		deleteAllZones()
	}
}

func deleteAllHosts() {
	p := fortigate.NewParam()
	result, err := conn.GetAddressObjects(p)
	if err != nil {
		fmt.Println("Failed to access Fortigate", err)
		os.Exit(1)
	}
	AddrCounter.Total = len(result)
	for _, v := range result {
		err = conn.DeleteAddress(v)
		if err == nil {
			AddrCounter.Success++
		} else {
			AddrCounter.Fail++
		}
	}
	fmt.Println("DeleteHosts (success/failure/total)", AddrCounter)
}

func deleteAllHostGroups() {
	p := fortigate.NewParam()
	result, err := conn.GetAddressGroups(p)
	if err != nil {
		fmt.Println("Aborting, failed to access Fortigate", err)
		os.Exit(1)
	}
	AddrGroupCounter.Total = len(result.Result)
	for _, v := range result.Result {
		_, err := conn.DeleteFortiObject(p, v)
		if err == nil {
			AddrGroupCounter.Success++
		} else {
			AddrGroupCounter.Fail++
		}
	}
	fmt.Println("DeleteHostGroups (success/failure/total)", AddrGroupCounter)
}

func deleteAllPolicy() {
	p := fortigate.NewParam()
	result, err := conn.GetFortiObject(p, fortigate.Policy{})
	if err != nil {
		fmt.Println("Aborting, failed to access Fortigate:", err)
		os.Exit(1)
	}
	// read policies
	var policies []fortigate.Policy
	err = fortigate.Interface2Object(result.Result, &policies)
	if err != nil {
		fmt.Println("Policy parsing failed:", err)
		os.Exit(1)
	}
	PolicyCounter.Total = len(policies)
	for _, v := range policies {
		_, err := conn.DeleteFortiObject(p, v)
		if err == nil {
			PolicyCounter.Success++
		} else {
			PolicyCounter.Fail++
		}
	}
	fmt.Println("DeletePolicy (success/failure/total)", PolicyCounter)
}

func deleteAllServiceGroups() {
	p := fortigate.NewParam()
	result, err := conn.GetFortiObject(p, fortigate.FirewallServiceGroup{})
	if err != nil {
		fmt.Println("Aborting, failed to access Fortigate:", err)
		os.Exit(1)
	}
	// read policies
	var res []fortigate.FirewallServiceGroup
	err = fortigate.Interface2Object(result.Result, &res)
	if err != nil {
		fmt.Println("AllServiceGroups parsing failed:", err)
		os.Exit(1)
	}
	ServiceGroupCounter.Total = len(res)
	for _, v := range res {
		_, err := conn.DeleteFortiObject(p, v)
		if err == nil {
			ServiceGroupCounter.Success++
		} else {
			ServiceGroupCounter.Fail++
		}
	}
	fmt.Println("DeleteServiceGroup (success/failure/total)", ServiceGroupCounter)
}

func deleteAllServices() {
	p := fortigate.NewParam()
	result, err := conn.GetFortiObject(p, fortigate.FirewallService{})
	if err != nil {
		fmt.Println("Aborting, failed to access Fortigate:", err)
		os.Exit(1)
	}
	// read policies
	var res []fortigate.FirewallService
	err = fortigate.Interface2Object(result.Result, &res)
	if err != nil {
		fmt.Println("AllServices parsing failed:", err)
		//TODO: there seems to be a bug in at least FortiOS 6.4.5, some ICMP service types (icmptype) are returned as string even the documentation states that it is of int. Until this is fixed I just have to ignore the any error from the parsing.
		// os.Exit(1)
	}
	ServiceCounter.Total = len(res)
	for _, v := range res {
		_, err := conn.DeleteFortiObject(p, v)
		if err == nil {
			ServiceCounter.Success++
		} else {
			ServiceCounter.Fail++
		}
	}
	fmt.Println("DeleteServices (success/failure/total)", ServiceCounter)
}

func deleteAllZones() {
	p := fortigate.NewParam()
	result, err := conn.GetFortiObject(p, fortigate.Zone{})
	if err != nil {
		fmt.Println("Aborting, failed to access Fortigate:", err)
		os.Exit(1)
	}
	// read policies
	var res []fortigate.Zone
	err = fortigate.Interface2Object(result.Result, &res)
	if err != nil {
		fmt.Println("Zones parsing failed:", err)
		os.Exit(1)
	}
	ZoneCounter.Total = len(res)
	for _, v := range res {
		_, err := conn.DeleteFortiObject(p, v)
		if err == nil {
			ZoneCounter.Success++
		} else {
			ZoneCounter.Fail++
		}
	}
	fmt.Println("DeleteZones (success/failure/total)", ZoneCounter)
}

func init() {
	version.NAME = "fg-deleteall"
	version.VERSION = fortigate.Version
}
