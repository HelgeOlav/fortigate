package main

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/utils/version"
	"flag"
	"fmt"
	"os"
)

var serverUrl *string // serverUrl is URL to the Fortigate, syntax "https://192.168.0.1"
var APIKey *string    // The Fortigate API key
var VDOM *string      // VDOM to back up
var saveTo *string    // output file name

var conn fortigate.Connection

func parseCmdline() {
	serverUrl = flag.String("url", "", "Syntax: https://my-fg")
	APIKey = flag.String("apikey", "", "API key")
	VDOM = flag.String("vdom", "", "VDOM to get config from")
	saveTo = flag.String("save", "", "filename to save to")
	flag.Parse()
	// check if commands are parsed
	if len(*serverUrl) == 0 || len(*APIKey) == 0 || len(*saveTo) == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	ver := version.Get()
	fmt.Println(ver.String())
	parseCmdline()
	conn = fortigate.Connection{
		BaseURL: *serverUrl + "/api/v2",
		Token:   *APIKey,
		VDOM:    *VDOM,
	}
	conn.IgnoreSSLCerts()
	// get configuration
	result, err := conn.GetConfiguration(len(*VDOM) > 0)
	if err != nil {
		fmt.Println("Getting configuration failed,", err)
		os.Exit(1)
	}
	fmt.Println("size of result:", len(result))
	// save to file
	f, err := os.Create(*saveTo)
	if err != nil {
		fmt.Println("Save error", err)
		os.Exit(1)
	}
	defer f.Close()
	_, err = f.Write(result)
	if err != nil {
		fmt.Println("Save failed", err)
		os.Exit(1)
	}
}

func init() {
	version.NAME = "fg-getconfig"
	version.VERSION = fortigate.Version
}
