# fg-getconfig

This is a backup tool, used to get the running configuration from a Fortigate.
You can either backup "global" or a VDOM, depending on your need. If you are
running multiple VDOMs, you should run this tool one time for each VDOM and then
one time without specifying a VDOM.

```shell
fg-getconfig -apikey xxx -url https://my-fg -vdom root -save savetofile
```
In case of any error the result code is 1. If the backup was ok then a result code of 0 is returned.