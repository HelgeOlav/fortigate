package fortigate

import (
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"net/http"
	"strconv"
)

// This file contains definitions to with address objects

const (
	AddrPath        = "cmdb/firewall/address" // path in API
	AddrTypeSubnet  = "ipmask"                // when type is subnet
	AddrTypeFQDN    = "fqdn"                  // when type is fqdn
	AddrTypeIpRange = "iprange"               // when type is an ip-range
	AddrTypeGeo     = "geography"             // when type is country
)

// Addr defines a Fortigate address object
type Addr struct {
	Name      string `json:"name,omitempty"`
	Origin    string `json:"q_origin_key,omitempty"`
	UUID      string `json:"uuid,omitempty"`
	Subnet    string `json:"subnet,omitempty"`   // for type=ipmask
	FQDN      string `json:"fqdn,omitempty"`     // for type=fqdn
	StartIP   string `json:"start-ip,omitempty"` // for type=iprange
	EndIP     string `json:"end-ip,omitempty"`   // for type=iprange
	Type      string `json:"type,omitempty"`     // [ ipmask(blank/default), iprange, fqdn, geography, wildcard, dynamic, interface-subnet, mac ]
	SubType   string `json:"sub-type,omitempty"`
	Country   string `json:"country,omitempty"` // for type=geography
	Interface string `json:"interface,omitempty"`
	ObjType   string `json:"obj-type,omitempty"`
	Comment   string `json:"comment,omitempty"`
	Color     int    `json:"color,omitempty"`
	TTL       int    `json:"cache-ttl,omitempty"`
}

func (a Addr) GetPreamble() []string {
	return []string{"config firewall address", "edit " + a.Name}
}

func (a Addr) GetEpilog() []string {
	return []string{"end"}
}

func (a Addr) GetConfigLines() []string {
	result := []string{}
	if len(a.Comment) > 0 {
		result = append(result, "set comment \""+a.Comment+"\"")
	}
	if len(a.Type) > 0 {
		result = append(result, "set type "+a.Type)
	}
	switch a.Type {
	case AddrTypeSubnet, "":
		if len(a.Subnet) > 0 {
			result = append(result, "set subnet "+a.Subnet)
		}
	case AddrTypeFQDN:
		if len(a.FQDN) > 0 {
			result = append(result, "set fqdn \""+a.FQDN+"\"")
		}
	case AddrTypeIpRange:
		if len(a.StartIP) > 0 && len(a.EndIP) > 0 {
			result = append(result, fmt.Sprintf("set start-ip %s\nset end-ip %s\n", a.StartIP, a.EndIP))
		}
	}
	if a.Color > 0 {
		result = append(result, "set color "+strconv.Itoa(a.Color))
	}
	return result
}

// GetMetaData implements FortiObjectReadable for Addr
func (a Addr) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: AddrPath}
}

// GetKey implements FortiObjectUpdatable for Addr
func (a Addr) GetKey() string {
	return a.Name
}

// AddrResult is the result from querying for addresses.
// Deprecated should be replaced by newer generic Forti... objects
type AddrResult struct {
	GenericResult
	Result []Addr `json:"results"`
}

func (c *Connection) GetAddressObjects(p Param) ([]Addr, error) {
	var result []Addr
	res, err := c.GetFortiObject(p, result)
	// now convert map into result
	if err == nil {
		config := &mapstructure.DecoderConfig{TagName: "json", Result: &result}
		decoder, err1 := mapstructure.NewDecoder(config)
		if err1 != nil {
			return result, err1
		}
		err = decoder.Decode(res.Result)
	}
	return result, err
}

// DeleteAddress deletes the address specified by Addr
func (c *Connection) DeleteAddress(a Addr) error {
	return c.DeleteAddressObject(a.Origin)
}

// DeleteAddressObject will delete the specified object
func (c *Connection) DeleteAddressObject(mkey string) error {
	if c == nil {
		return errors.New("DeleteAddressObject Connection==nil")
	}
	p := NewParam().SetSuffix("/" + mkey)
	req, err := c.NewRequest(http.MethodDelete, AddrPath, p, nil)
	if err != nil {
		return err
	}
	res, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	_ = res.Body.Close()
	if res.StatusCode == http.StatusOK {
		return nil
	}
	return errors.New("DeleteAddressObject failed somehow") //TODO: perhaps fix error handling?
}

// CreateAddressObject create a new address object specified by a
func (c *Connection) CreateAddressObject(a Addr) error {
	if c == nil {
		return errors.New("CreateAddressObject Connection==nil")
	}
	var result GenericResult
	err := c.DoAndParseRequest(AddrPath, http.MethodPost, NewParam(), a, &result)
	if err != nil {
		return err
	}
	if result.HTTPResponseCode != http.StatusOK {
		errMsg := strconv.Itoa(result.HTTPResponseCode) + " " + result.Status + " " + strconv.Itoa(result.Error)
		return errors.New(errMsg)
	}
	return nil
}

// UpdateAddressObject updates an existing address object
func (c *Connection) UpdateAddressObject(a Addr) error {
	if c == nil {
		return errors.New("CreateAddressObject Connection==nil")
	}
	var result GenericResult
	err := c.DoAndParseRequest(AddrPath, http.MethodPut, NewParam().SetMkey(a.Origin), a, &result)
	if err != nil {
		return err
	}
	if result.HTTPResponseCode != http.StatusOK {
		errMsg := strconv.Itoa(result.HTTPResponseCode) + " " + result.Status + " " + strconv.Itoa(result.Error)
		return errors.New(errMsg)
	}
	return nil
}
