package fortigate

import (
	"errors"
	"io/ioutil"
	"net/http"
)

// Back up configuration

const BackupPath = "monitor/system/config/backup"

// GetConfiguration returns the running configuration. If vdom is empty global is used, otherwise a vdom is returned.
func (c *Connection) GetConfiguration(vdom bool) ([]byte, error) {
	var p Param
	if vdom == false {
		p = NewParam().Generic("scope", "global")
	}
	if vdom == true {
		p = NewParam().Generic("scope", "vdom")
	}
	// create request object
	req, err := c.NewRequest(http.MethodGet, BackupPath, p, nil)
	if err != nil {
		return nil, err
	}
	// do request
	resp, err := c.Client.Do(req)
	// read response
	resultBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = errors.New("got HTTP status " + resp.Status)
	}
	return resultBody, err
}
