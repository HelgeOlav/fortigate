package fortigate

// Defines proxy related objects. At this point it seems like the proxy-policy API is broken, at least for FortiOS 6.4.5
// as I am not able to POST any valid service reference using the REST API. DELETE and GET works.

import (
	"errors"
	"net/http"
	"strconv"
)

const WebProxyPolicyPath = "cmdb/firewall/proxy-policy"
const WebProxyGlobalSettingsPath = "cmdb/web-proxy/global"
const WebProxyExplicitSettingsPath = "cmdb/web-proxy/explicit"

type WebProxyPolicy struct {
	PolicyId   int            `json:"policyid"`
	UUID       string         `json:"uuid,omitempty"`
	Name       string         `json:"name,omitempty"`
	OriginKey  int            `json:"q_origin_key,omitempty"`
	SrcIntf    []LinkedObjRef `json:"srcintf,omitempty"`
	DstIntf    []LinkedObjRef `json:"dstintf,omitempty"`
	PolicyID   int            `json:"policyid,omitempty"`
	Proxy      string         `json:"string"` //type of proxy; [ explicit-web, transparent-web, ftp, ssh, ssh-tunnel, wanopt ]
	SrcAddr    []LinkedObjRef `json:"srcaddr,omitempty"`
	DstAddr    []LinkedObjRef `json:"dstaddr,omitempty"`
	Service    []LinkedObjRef `json:"service,omitempty"`
	Action     string         `json:"action,omitempty"`     // [ deny, allow ]
	Schedule   string         `json:"schedule,omitempty"`   // always
	Status     string         `json:"status,omitempty"`     // [ enable, disable ]
	LogTraffic string         `json:"logtraffic,omitempty"` // [ all, utm, disable ]
	SrcAddr6   []LinkedObjRef `json:"srcaddr6,omitempty"`
	DstAddr6   []LinkedObjRef `json:"dstaddr6,omitempty"`
	PoolName   []LinkedObjRef `json:"poolname,omitempty"`
	Comment    string         `json:"comments"`
	SessionTTL int            `json:"session-ttl,omitempty"`
}

// WebPolicyGlobalSettings represents global settings. This setting can only GET and PUT, delete and POST is not supported.
// TODO: implement FortiObject*
type WebPolicyGlobalSettings struct {
	SSLCert         string `json:"ssl-cert,omitempty"`
	SSLCACert       string `json:"ssl-ca-cert,omitempty"`
	FastPolicyMatch string `json:"fast-policy-match,omitempty"` // [ enable, disable ]
	ProxyFQDN       string `json:"proxy-fqdn,omitempty"`
	MaxReqLen       int    `json:"max-request-length,omitempty"`
	MaxMsgLen       int    `json:"max-message-length,omitempty"`
}

type WebPolicyExplicitSettings struct {
	Status      string `json:"status,omitempty"`        // [ enable, disable ]
	FtpOverHttp string `json:"ftp-over-http,omitempty"` // [ enable, disable ]
	Socks       string `json:"socks,omitempty"`         // [ enable, disable ]
	HttpPort    string `json:"http-incoming-port,omitempty"`
	HttpsPort   string `json:"https-incoming-port,omitempty"`
	IncomingIP  string `json:"incoming-ip,omitempty"`
	OutgoingIP  string `json:"outgoing-ip,omitempty"`
	IPv6Status  string `json:"ipv6-status,omitempty"` // [ enable, disable ]
	IncomingIP6 string `json:"incoming-ip6,omitempty"`
	OutgoingI6  string `json:"outgoing-ip6,omitempty"`
}

func (w WebPolicyExplicitSettings) GetKey() string {
	return ""
}

func (w WebPolicyExplicitSettings) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{
		BasePath:        WebProxyExplicitSettingsPath,
		ParamDeleteHook: methodNotAllowed,
		ParamCreateHook: methodNotAllowed,
	}
}

func (w WebProxyPolicy) GetKey() string {
	return strconv.Itoa(w.OriginKey)
}

func (w WebProxyPolicy) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: WebProxyPolicyPath}
}

func (res *FortigateResult) GetWebproxyPolicyFromResult() (WebProxyPolicy, error) {
	pol, err := res.GetWebProxyPoliciesFromResult()
	if len(pol) == 1 {
		return pol[0], err
	}
	if err != nil {
		return WebProxyPolicy{}, err
	}
	return WebProxyPolicy{}, errors.New("Incorrect result from FortigateResult")
}

// FortigateResult converts a result into an array of policies
func (res *FortigateResult) GetWebProxyPoliciesFromResult() ([]WebProxyPolicy, error) {
	if res == nil {
		return []WebProxyPolicy{}, errors.New("nil GetWebProxyPoliciesFromResult")
	}
	var pol []WebProxyPolicy
	if res.HTTPResponseCode != http.StatusOK || res.Name != "proxy-policy" {
		return pol, errors.New("GetWebProxyPoliciesFromResult not from policy")
	}
	err := Interface2Object(res.Result, &pol)
	return pol, err
}
