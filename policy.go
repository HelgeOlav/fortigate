package fortigate

import (
	"errors"
	"net/http"
	"strconv"
)

// Policy struct specifies a firewall policy
type Policy struct {
	PolicyId        int            `json:"policyid"`
	OriginKey       int            `json:"q_origin_key,omitempty"`
	UUID            string         `json:"uuid,omitempty"`
	Name            string         `json:"name"`
	Status          string         `json:"status,omitempty"`
	SrcIntf         []LinkedObjRef `json:"srcintf,omitempty"`
	DstIntf         []LinkedObjRef `json:"dstintf,omitempty"`
	SrcAddr         []LinkedObjRef `json:"srcaddr,omitempty"`
	DstAddr         []LinkedObjRef `json:"dstaddr,omitempty"`
	SrcAddr6        []LinkedObjRef `json:"srcaddr6,omitempty"`
	DstAddr6        []LinkedObjRef `json:"dstaddr6,omitempty"`
	InternetService string         `json:"internet-service,omitempty"` // disable or enable
	Action          string         `json:"action,omitempty"`
	Schedule        string         `json:"schedule,omitempty"`
	Service         []LinkedObjRef `json:"service,omitempty"`
	LogTraffic      string         `json:"logtraffic,omitempty"` // One of [ all, utm, disable ]
	NAT             string         `json:"nat,omitempty"`
	IPPool          string         `json:"ippool,omitempty"`
	PoolName        []LinkedObjRef `json:"poolname,omitempty"`
	PoolName6       []LinkedObjRef `json:"poolname6,omitempty"`
	Comment         string         `json:"comments"`
}

const PolicyPath = "cmdb/firewall/policy"

func (p Policy) GetKey() string {
	return strconv.Itoa(p.PolicyId)
}

func (p Policy) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: PolicyPath}
}

func (res *FortigateResult) GetPolicyFromResult() (Policy, error) {
	pol, err := res.GetPoliciesFromResult()
	if len(pol) == 1 {
		return pol[0], err
	}
	if err != nil {
		return Policy{}, err
	}
	return Policy{}, errors.New("Incorrect result from FortigateResult")
}

// FortigateResult converts a result into an array of policies
func (res *FortigateResult) GetPoliciesFromResult() ([]Policy, error) {
	if res == nil {
		return []Policy{}, errors.New("nil FortigateResult")
	}
	var pol []Policy
	if res.HTTPResponseCode != http.StatusOK || res.Name != "policy" {
		return pol, errors.New("FortigateResult not from policy")
	}
	err := Interface2Object(res.Result, &pol)
	return pol, err
}
