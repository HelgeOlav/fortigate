package fortigate

// This file will assist on generating CLI output

//TODO: not done yet

type FgCliOutput interface {
	GetPreamble() []string    // header lines (config system interface / ...)
	GetEpilog() []string      // trailing lines (next / end)
	GetConfigLines() []string // get the configuration lines
}
