package fortigate

import (
	"errors"
	"net/http"
	"strconv"
)

// This file contains what you need to query current sessions on the firewall.
// There are lots of ways to query for just the data you need.
// The sessions query will fail unless you specify a mandatory count,
// which defaults to 20 if you did not specify anything.

const SessionsPath = "monitor/firewall/session"

// SessionsSummary is a summary of firewall sessions. Note that most of these fields are optional,
// a count of 0 can be an indication of the field being absent.
type SessionsSummary struct {
	MatchedCount       int `json:"matched_count"`
	SessionCount       int `json:"session_count"`
	MatchedNpuCount    int `json:"matched_npu_count"`
	NpuSessionCount    int `json:"npu_session_count"`
	MatchedNturboCount int `json:"matched_nturbo_count"`
	NturboSessionCount int `json:"nturbo_session_count"`
	SetupRate          int `json:"setup_rate"`
}

// SessionDetail is information about one session
type SessionDetail struct {
	Proto    string `json:"proto"` // TCP, UDP or number
	Srcintf  string `json:"srcintf"`
	Saddr    string `json:"saddr"`
	Sport    int    `json:"sport"`
	Dstintf  string `json:"dstintf"`
	Daddr    string `json:"daddr"`
	Dport    int    `json:"dport"`
	PolicyID int    `json:"policyid"`
	VDOM     string `json:"vf"`
	Expiry   string `json:"expiry"`
	Duration int    `json:"duration"`
}

// SessionResultResult The result inside the result, eh...
type SessionResultResult struct {
	Summary  SessionsSummary `json:"summary"`
	Sessions []SessionDetail `json:"details"`
}

// setSessionDefaultCount sets a session count unless set from before
func setSessionDefaultCount(p Param) (Param, error) {
	return p.DefaultValue("count", "10"), nil
}

// GetMetaData provides the data needed by Forti* calls
func (s SessionResultResult) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: SessionsPath, ParamGetHook: setSessionDefaultCount}
}

// SessionResult he result we get back from the query
// Deprecated: Phased out, use the new Forti* calls
type SessionResult struct {
	GenericResult
	SessionResultResult `json:"results"`
}

// GetSessions returns all sessions from the firewall, limited down to search and query options.
// There are many ways to search for sessions, read the documentation
// To add custom searches use Param.Generic(k, v)
func (c *Connection) GetSessions(p Param) (SessionResult, error) {
	var result SessionResult
	err := c.DoAndParseRequest(SessionsPath, http.MethodGet, p, nil, &result)
	if err == nil {
		if result.HTTPResponseCode != http.StatusOK {
			err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
		}
	}
	return result, err
}

// Count sets the count query parameter, limiting the maximum numbers of results back
func (p Param) Count(c int) Param {
	p.QueryParams["count"] = strconv.Itoa(c)
	return p
}

// Summary is used with Sessions to indicate that you want the optional summary back in the response
func (p Param) Summary() Param {
	p.QueryParams["summary"] = "true"
	return p
}

// Policy matches session by this policy ID
func (p Param) Policy(id int) Param {
	p.QueryParams["policyid"] = strconv.Itoa(id)
	return p
}
