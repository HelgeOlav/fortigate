package fortigate

const ZonePath = "cmdb/system/zone"

// Zone is a zone on the firewall
type Zone struct {
	Name        string         `json:"name,omitempty"`
	Description string         `json:"description,omitempty"`
	Origin      string         `json:"q_origin_key,omitempty"`
	Intrazone   string         `json:"intrazone"` // allow or deny
	Interface   []LinkedObjRef `json:"interface"`
}

func (z Zone) GetKey() string {
	return z.Name
}

func (z Zone) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: ZonePath}
}

// SetInterfaces sets the interface list from the input array string
func (z *Zone) SetInterfaces(ifList []string) {
	if z != nil {
		z.Interface = NewIfLinkedObjRefFromStrings(ifList)
	}
}

// GetZonesAsIfMap returns a map where key is interface and value is zone.
func GetZonesAsIfMap(zones []Zone, ifs []Interface) map[string]string {
	result := make(map[string]string)
	for _, v := range ifs {
		result[v.Name] = v.Name // default to no zone
		for _, z1 := range zones {
			for _, z := range z1.Interface {
				if z.IfName == v.Name {
					result[v.Name] = z1.Name
				}
			}
		}
	}
	return result
}
