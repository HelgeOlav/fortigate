package fortigate

// Services and Service Groups

const ServicePath = "cmdb/firewall.service/custom"
const ServiceGroupPath = "cmdb/firewall.service/group"
const ServiceTCPUDP = "TCP/UDP/SCTP" // service name for Protocol below
const ServiceICMP = "ICMP"

// FirewallServiceGroup represents a service group object
type FirewallServiceGroup struct {
	Name      string         `json:"name,omitempty"`
	Comment   string         `json:"comment,omitempty"`
	Color     int            `json:"color,omitempty"`
	OriginKey string         `json:"q_origin_key,omitempty"`
	Member    []LinkedObjRef `json:"member"`
}

func (f FirewallServiceGroup) GetKey() string {
	return f.Name
}

func (f FirewallServiceGroup) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: ServiceGroupPath}
}

// FirewallService defines a service object
type FirewallService struct {
	Name          string `json:"name,omitempty"`
	Comment       string `json:"comment,omitempty"`
	Color         int    `json:"color,omitempty"`
	OriginKey     string `json:"q_origin_key,omitempty"`
	Category      string `json:"category,omitempty"`
	Protocol      string `json:"protocol,omitempty"`       // "ICMP", "TCP/UDP/SCTP", "ICMP6"
	TcpPortRange  string `json:"tcp-portrange,omitempty"`  // if Protocol == "TCP/UDP/SCTP", multiple ports separated by space and port ranges as from-to. Eg "80 443-444"
	UdpPortRange  string `json:"udp-portrange,omitempty"`  // if Protocol == "TCP/UDP/SCTP", multiple ports separated by space and port ranges as from-to. Eg "80 443-444"
	SctpPortRange string `json:"sctp-portrange,omitempty"` // if Protocol == "TCP/UDP/SCTP", multiple ports separated by space and port ranges as from-to. Eg "80 443-444"
	IcmpType      int    `json:"icmptype,omitempty"`       // if Protocol == ICMP (are also stored as string)
	IcmpCode      string `json:"icmpcode,omitempty"`       // if Protocol == ICMP
}

func (f FirewallService) GetKey() string {
	if len(f.OriginKey) > 0 {
		return f.OriginKey
	}
	return f.Name
}

func (f FirewallService) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: ServicePath}
}
