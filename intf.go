package fortigate

import (
	"errors"
	"net/http"
	"strconv"
)

const IntfPath = "cmdb/system/interface"

// Interface defines an interface on the Fortigate
type Interface struct {
	Name               string `json:"name,omitempty"`
	Origin             string `json:"q_origin_key,omitempty"`
	Description        string `json:"description,omitempty"`
	UUID               string `json:"uuid,omitempty"`
	VDOM               string `json:"vdom"`
	VRF                int    `json:"string,omitempty"`
	Fortilink          string `json:"fortilink,omitempty"` // enable or disable
	Mode               string `json:"mode"`                // [ static, dhcp, pppoe ]
	Distance           int    `json:"distance"`
	Priority           int    `json:"priority"`
	DhcpRelayService   string `json:"dhcp-relay-service,omitempty"` // enable or disable
	DhcpRelayInterface string `json:"dhcp-relay-interface,omitempty"`
	DhcpRelayIP        string `json:"dhcp-relay-ip,omitempty"`   // IP addresses as string with quotes and space between
	DhcpRelayType      string `json:"dhcp-relay-type,omitempty"` // like regular
	IP                 string `json:"ip"`                        // 1.1.1.1 255.255.255.0
	AllowAccess        string `json:"allowaccess"`               // Name of services with small letters and space between like "snmp ping"
	Status             string `json:"status"`                    // up or down
	Type               string `json:"type"`                      // physical, vlan etc. [ physical, vlan, aggregate, redundant, tunnel, vdom-link, loopback, switch, hard-switch, vap-switch, wl-mesh, fext-wan, vxlan, geneve, hdlc, switch-vlan, emac-vlan ]
	MTU                int    `json:"mtu,omitempty"`
	VlanID             int    `json:"vlanid,omitempty"`        // id of VLAN subtype interface
	VlanProtocol       string `json:"vlan-protocol,omitempty"` // 8021q
	Role               string `json:"role,omitempty"`          // [ lan, wan, dmz, undefined ]
	SnmpIndex          int    `json:"snmp-index,omitempty"`
	Color              int    `json:"color"`
	Interface          string `json:"interface,omitempty"` // name of parent interface
}

func (i Interface) GetKey() string {
	return i.Name
}

func (i Interface) GetMetaData() FortiObjectMetadata {
	return FortiObjectMetadata{BasePath: IntfPath}
}

// IntfResult result from getting all interfaces
type IntfResult struct {
	GenericResult
	Result []Interface `json:"results"`
}

// FindByName look up a interface by its name
func (a IntfResult) FindByName(name string) Interface {
	res := Interface{}
	for _, v := range a.Result {
		if v.Name == name {
			return v
		}
	}
	return res
}

// GetIntfs returns a list of interfaces from the firewall
// Param can be used to scope down what we get back, if empty we get everything
func (c *Connection) GetIntfs(p Param) (IntfResult, error) {
	var result IntfResult
	err := c.DoAndParseRequest(IntfPath, http.MethodGet, p, nil, &result)
	if err == nil {
		if result.HTTPResponseCode != http.StatusOK {
			err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
		}
	}
	return result, err
}
