package fortigate

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"github.com/mitchellh/mapstructure"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strconv"
)

// Connection defines values that we need to connect to the Fortigate
type Connection struct {
	BaseURL     string      `json:"baseurl"`      // base URL for calling the Fortigate
	VDOM        string      `json:"vdom"`         // the VDOM we are working on
	Token       string      `json:"token"`        // API token
	Client      http.Client `json:"-"`            // http client used for connecting
	InsecureSSL bool        `json:"insecure-ssl"` // set to disable SSL warnings - only checked in DefaultConnection() and LoadConnection()
}

// DefaultConnection tries to create a connection object with defaults. The defaults are to use environment variables or
// look into the current directory after the file conn.json
func DefaultConnection() (Connection, error) {
	hostName := os.Getenv("FORTIOS_ACCESS_HOSTNAME")
	if len(hostName) > 0 {
		conn := Connection{
			BaseURL:     "https://" + hostName + "/api/v2",
			VDOM:        "root",
			Token:       os.Getenv("FORTIOS_ACCESS_TOKEN"),
			InsecureSSL: os.Getenv("FORTIOS_INSECURE") == "true",
		}
		if conn.InsecureSSL {
			conn.IgnoreSSLCerts()
		}
		return conn, nil
	}
	return LoadConnection("conn.json")
}

// LoadConnection creates a connection object from file
func LoadConnection(fn string) (Connection, error) {
	var conn Connection
	byte, err := ioutil.ReadFile(fn)
	if err == nil {
		err = json.Unmarshal(byte, &conn)
		if err == nil && conn.InsecureSSL {
			conn.IgnoreSSLCerts()
		}
	}
	return conn, err
}

// NewParam creates a new empty Parameter set, used to construct requests.
func NewParam() Param {
	p := Param{map[string]string{}, "", context.Background()}
	return p
}

// Param contains data for the specific request like query parameters and context.
// There are many helper methods for setting the parameters that you need.
type Param struct {
	QueryParams   map[string]string
	RequestSuffix string
	Ctx           context.Context // context passed to http
}

// Token add access token to parameter string.
func (p Param) Token(token string) Param {
	p.QueryParams["access_token"] = token
	return p
}

// Generic set generic parameter string.
func (p Param) Generic(k, v string) Param {
	p.QueryParams[k] = v
	return p
}

// VDOM specify VDOM.
func (p Param) VDOM(vdom string) Param {
	if len(vdom) > 0 {
		p.QueryParams["vdom"] = vdom
	}
	return p
}

// Context set context used for the HTTP call.
func (p Param) Context(c context.Context) Param {
	p.Ctx = c
	return p
}

// SetSuffix set the suffix added to the request URL
func (p Param) SetSuffix(suffix string) Param {
	p.RequestSuffix = suffix
	return p
}

// SetMkey sets RequestSuffix to /<key>
func (p Param) SetMkey(key string) Param {
	p.RequestSuffix = "/" + key
	return p
}

// Format add format specifier
func (p Param) Format(format string) Param {
	if _, ok := p.QueryParams["format"]; ok {
		// we have to append
		p.QueryParams["format"] = p.QueryParams["format"] + "+" + format
	} else {
		// first format specifier
		p.QueryParams["format"] = format
	}
	return p
}

// DefaultValue sets a key/value if it is not present already
func (p Param) DefaultValue(k, v string) Param {
	if _, ok := p.QueryParams[k]; !ok {
		p.QueryParams[k] = v
	}
	return p
}

// NewRequest creates generic HTTP requests to the Fortigate using all the parameters specified
// In return you get a Request object that need to complete and close when you are done. The URL is modified
// and the suffix from Param is added here.
func (c *Connection) NewRequest(method, path string, p Param, body io.Reader) (*http.Request, error) {
	if c == nil {
		return nil, errors.New("nil Connection to NewGetRequest")
	}
	// set our own parameters
	p = p.Token(c.Token).VDOM(c.VDOM)
	// build query values
	query := url.Values{}
	for k, v := range p.QueryParams {
		query.Set(k, v)
	}
	// construct URL
	paramUrl := c.BaseURL + "/" + path + p.RequestSuffix
	// create a request
	req, err := http.NewRequestWithContext(p.Ctx, method, paramUrl, body)
	req.URL.RawQuery = query.Encode()
	req.Header.Set("Content-Type", "application/json")
	return req, err
}

// GetAndParseRequest will do a complete HTTP GET request and unmarshal the result into result
// Deprecated: Use DoAndParseRequest instead
func (c *Connection) GetAndParseRequest(path string, p Param, result interface{}) error {
	// nil check
	if c == nil {
		return errors.New("nil Connection to GetAndParseRequest")
	}
	// create request object
	req, err := c.NewRequest(http.MethodGet, path, p, nil)
	if err != nil {
		return err
	}
	// do request
	resp, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// read response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	// parse the response and return
	err = json.Unmarshal(body, result)
	return err
}

// DoAndParseRequest will handle any HTTP method and parse the result into result.
func (c *Connection) DoAndParseRequest(path, method string, p Param, body interface{}, result interface{}) error {
	// nil check
	if c == nil {
		return errors.New("nil Connection to DoAndParseRequest")
	}
	// marshal body if present
	var bodyReader io.Reader = nil
	if body != nil {
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			return err
		}
		bodyReader = bytes.NewReader(bodyBytes)
	}
	// create request object
	req, err := c.NewRequest(method, path, p, bodyReader)
	if err != nil {
		return err
	}
	// do request
	resp, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// read response
	resultBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	// parse the response and return
	err = json.Unmarshal(resultBody, result)
	// error on non 2xx response
	if err == nil && (resp.StatusCode < 200 || resp.StatusCode > 299) {
		return errors.New(req.URL.Path + " " + resp.Status)
	}
	return err
}

// IgnoreSSLCerts call this helper method to disable SSL certificate warnings
func (c *Connection) IgnoreSSLCerts() {
	if c != nil {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		c.Client.Transport = tr
	}
}

// GetFortiObjectReadableMetadata takes an input of either a Fortigate object or an array of the objects
// and returns true if it has implemented FortiObjectReadable.
func GetFortiObjectReadableMetadata(input interface{}) (bool, FortiObjectMetadata) {
	if input != nil {
		typ := reflect.TypeOf(input)
		if typ.Kind() == reflect.Slice || typ.Kind() == reflect.Array {
			input = reflect.New(typ.Elem()).Interface()
		}
		if v, ok := input.(FortiObjectReadable); ok {
			return true, v.GetMetaData()
		}
	}
	return false, FortiObjectMetadata{}
}

// GetFortiObjectUpdatableMetadata takes an input of a Fortigate object
// and returns true if it has implemented FortiObjectUpdatable.
func GetFortiObjectUpdatableMetadata(input interface{}) (bool, FortiObjectMetadata) {
	if input != nil {
		typ := reflect.TypeOf(input)
		if typ.Kind() == reflect.Slice || typ.Kind() == reflect.Array {
			input = reflect.New(typ.Elem()).Interface()
		}
		if v, ok := input.(FortiObjectUpdatable); ok {
			return true, v.GetMetaData()
		}
	}
	return false, FortiObjectMetadata{}
}

// GetFortiObject is a generic way to retrieve data from the Fortigate. In object you need to pass
// a struct that is the result from the GET operation that fits into Result. object can be either an array or
// just a struct depending on what data you expect back. object has to implement FortiObjectReadable for this call
// to succeed.
// Note that even when you pass object in, the result in FortigateResult.Result will be of type map[string]interface{}
func (c *Connection) GetFortiObject(p Param, object interface{}) (FortigateResult, error) {
	var result FortigateResult
	var err error
	result.Result = object
	isReadable, md := GetFortiObjectReadableMetadata(object)
	if isReadable {
		url := md.BasePath
		if md.ParamGetHook != nil {
			p, err = md.ParamGetHook(p)
			if err != nil {
				return result, err
			}
		}
		err = c.DoAndParseRequest(url, http.MethodGet, p, nil, &result)
		if err == nil {
			if result.HTTPResponseCode != http.StatusOK {
				// err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
				err = result.errorIfError() // TODO: move out of if-block
			}
		}
	} else {
		return result, errors.New("FortiObjectReadable not passed into GetFortiObject")
	}
	return result, err
}

// DeleteFortiObject deletes an object from the Fortigate. The object passed in has to be one object and it has to
// implement FortiObjectUpdatable in order to work. FortigateResult.Result is returned of type map[string]interface{}
func (c *Connection) DeleteFortiObject(p Param, object interface{}) (FortigateResult, error) {
	var result FortigateResult
	var err error
	result.Result = object
	isReadable, md := GetFortiObjectUpdatableMetadata(object)
	if isReadable {
		url := md.BasePath
		mkey := object.(FortiObjectUpdatable).GetKey()
		p = p.SetMkey(mkey)
		if md.ParamDeleteHook != nil {
			p, err = md.ParamGetHook(p)
			if err != nil {
				return result, err
			}
		}
		err = c.DoAndParseRequest(url, http.MethodDelete, p, nil, &result)
		if err == nil {
			if result.HTTPResponseCode != http.StatusOK {
				err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
			}
		}
	} else {
		return result, errors.New("FortiObjectReadable not passed into DeleteFortiObject")
	}
	return result, err
}

// UpdateFortiObject updates an existing object.
func (c *Connection) UpdateFortiObject(p Param, object interface{}) (FortigateResult, error) {
	var result FortigateResult
	var err error
	result.Result = object
	isReadable, md := GetFortiObjectUpdatableMetadata(object)
	if isReadable {
		url := md.BasePath
		mkey := object.(FortiObjectUpdatable).GetKey()
		p = p.SetMkey(mkey)
		if md.ParamUpdateHook != nil {
			p, err = md.ParamGetHook(p)
			if err != nil {
				return result, err
			}
		}
		err = c.DoAndParseRequest(url, http.MethodPut, p, nil, &result)
		if err == nil {
			if result.HTTPResponseCode != http.StatusOK {
				err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
			}
		}
	} else {
		return result, errors.New("FortiObjectReadable not passed into UpdateFortiObject")
	}
	return result, err
}

// CreateFortiObject creates new objects.
func (c *Connection) CreateFortiObject(p Param, object interface{}) (FortigateResult, error) {
	var result FortigateResult
	var err error
	result.Result = object
	isReadable, md := GetFortiObjectUpdatableMetadata(object)
	if isReadable {
		url := md.BasePath
		if md.ParamCreateHook != nil {
			p, err = md.ParamGetHook(p)
			if err != nil {
				return result, err
			}
		}
		err = c.DoAndParseRequest(url, http.MethodPost, p, object, &result)
		if err == nil {
			if result.HTTPResponseCode != http.StatusOK {
				err = errors.New(strconv.Itoa(result.HTTPResponseCode) + " " + result.Status)
			}
		}
	} else {
		return result, errors.New("FortiObjectReadable not passed into CreateFortiObject")
	}
	return result, err
}

// Interface2Object helper method to convert anything to an object as long as it can be parsed.
// Used to convert map[string]interface{} from generic GET commands
func Interface2Object(input interface{}, result interface{}) error {
	config := &mapstructure.DecoderConfig{TagName: "json", Result: result}
	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return err
	}
	return decoder.Decode(input)
}
